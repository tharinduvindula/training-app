import React, {  } from 'react';
import './login.css';
import { Form, Input, Button, Checkbox } from 'antd';
import 'antd/dist/antd.css';
import history from "../../utils/history";
import axios from 'axios';
import jwt from 'jwt-decode';
import { getElementError } from '@testing-library/react';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};
interface LoginComponentProps {
    onChildClick: (val:boolean) => void,
    onChild2Click: (val: boolean) => void
}

const Login: React.FC<LoginComponentProps> = ({ onChildClick, onChild2Click }) => {
    const onFinish = (values: any) => {
        console.log('Success:', values);
        const user = {
            email: values.username,
            password: values.password
        };
        axios.post(`http://localhost:3001/api/users`, user)
            .then((res) => {
                console.log(res.data);
                if (res.data.success == true){
                    const decodedToken:any = jwt(res.data.userToken.split('JWT')[1]);
                    var dateNow = new Date();
                    console.log(decodedToken.email)
                    console.log(dateNow.getTime()/1000)
                    console.log(decodedToken.exp)
                    if (decodedToken.exp > dateNow.getTime()/1000 && decodedToken.email === user.email){
                        localStorage.setItem('token', res.data.userToken);
                        onChild2Click(false);
                    } else {
                        alert('login error')
                    }
                    
                    // history.push("/home");
                } else {
                    alert('user not exsit Password incorect')
                }
                
            }).catch((e) => {
                console.log(e)
                alert("user not exsit Password incorect")
            })
        
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    setTimeout(() => {
       
    }, 500);

    return (
        <div className="container2">
            <div style={{ flex: 4 }}></div>
            <div style={{flex:2, textAlign: "center" }}><h2 className="loginHaderText">Login Form</h2></div>
            <div style={{ flex: 2, textAlign: "center"}}>
                <Form 
                    {...layout}
                    name="basic"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item 
                        
                        name="username"
                        rules={[{type:"email", required: true, message: 'Please input valid Email!' }]}
                    >
                        <Input  placeholder="Email" />
                    </Form.Item>

                    <Form.Item 
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password placeholder="Password" />
                    </Form.Item>

                    <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button className="loginBtn" type="primary" htmlType="submit">
                           Login
                        </Button>
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button 
                            onClick={() => onChildClick(false)}
                            // onClick={() => props.handleClick(false)}
                            className="registerBtn" type="primary" htmlType="button">
                            Register
                        </Button>
                    </Form.Item>
                </Form>

            </div>
            <div style={{ flex: 4 }}></div>
        </div>
        );
};

// ReactDOM.render(<Login />, mountNode);


export default Login;