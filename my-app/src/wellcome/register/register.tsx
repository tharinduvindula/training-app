import React, { FC, useState } from 'react';
import './register.css';
import { Form, Input, Button, Checkbox, Select } from 'antd';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import axios from 'axios';

const { Option } = Select;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

interface RegisterComponentProps {
    onChildClick: (val: boolean) => void
}

const Register: React.FC<RegisterComponentProps> = ({ onChildClick }) => {
    const onFinish = (values: any) => {
        console.log('Success:', values);

        if (values.comfirmPassword == values.password){
            const user = {
                email: values.email,
                name: values.name,
                password: values.password
            };
            axios.post(`http://localhost:3001/api/users`,  user)
                .then((res) => {
                    console.log(res.data);
                    alert("user added");
                    onChildClick(true);
                }).catch(()=>{
                    alert("user exsit")
                })
        } else {
            alert("comfirmPassword and password dose not match")
        }

        
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };





    return (
        <div className="container2">
            <div style={{ flex: 4 }}></div>
            <div style={{ flex: 2, textAlign: "center" ,paddingTop:"20%"}}><h2 className="loginHaderText">Register Form</h2></div>
            <div style={{ flex: 2, textAlign: "center" }}>
                <Form
                    {...layout}
                    name="basic"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item

                        name="name"
                        rules={[{ required: true, message: 'Please input Your name!' }]}
                    >
                        <Input placeholder="Name" />
                    </Form.Item>

                    <Form.Item

                        name="email"
                        rules={[{ type: "email", required: true, message: 'Please input valid Email!' }]}
                    >
                        <Input placeholder="Email" />
                    </Form.Item>

                    {/* <Form.Item

                        name="age"
                        rules={[{ required: true, message: 'Please input valid Age!' }]}
                    >
                        <Input type="number" placeholder="Age" />
                    </Form.Item>

                    <Form.Item

                        name="gender"
                        rules={[{ required: true, message: 'Please input Your gender!' }]}
                    >   
                        <Select placeholder="Gender" style={{ width: "100%" }} onChange={handleChange}>
                            <Option value="male">Male</Option>
                            <Option value="female">Female</Option>
                        </Select>
                    
                    </Form.Item>

                    <Form.Item

                        name="address"
                        rules={[{  required: true, message: 'Please input your Address!' }]}
                    >
                        <Input placeholder="Address" />
                    </Form.Item>

                    <Form.Item

                        name="dateOfBirth"
                        rules={[{ type: "date", required: true, message: 'Please input valid Date!( mm/dd/yyyy)' }]}
                    >
                        <Input placeholder="Date of Birth (mm/dd/yyyy)" />
                    </Form.Item> */}
                    

                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password placeholder="Password" />
                    </Form.Item>

                    <Form.Item
                        name="comfirmPassword"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password placeholder="comfirm Password" />
                    </Form.Item>

                    <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button className="loginBtn" type="primary" htmlType="submit">
                            Register
                        </Button>
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button
                            onClick={() => onChildClick(true)}
                            className="registerBtn" type="primary"  htmlType="button">
                            Login
                        </Button>
                    </Form.Item>
                </Form>

            </div>
            <div style={{ flex: 4 }}></div>
        </div>
    );
};



export default Register;