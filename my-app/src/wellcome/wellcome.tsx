import React, { FC, Props, useState } from 'react';
import './wellcome.css';
import back from '../assets/back.jpg'
import Login from './login/login';
import Register from './register/register';

interface WellcomeComponentProps {
    onChild1Click: (val: boolean) => void
}

const Wellcome: FC<WellcomeComponentProps> = ({ onChild1Click}) => {
    const [loginForm, setLoginForm] = useState(true);
    const handleClick = (val: React.SetStateAction<boolean>) => {
        setLoginForm(val);
    };
    return (
        //  image div 
        <div className="container">
            <div className="imgContainer">
                <img src={back} className="wellcomeImg" alt="" />
            </div>
            <div className="formContainer" >
                
                {loginForm ? 
                <Login onChildClick={handleClick} onChild2Click={onChild1Click} /> : <Register onChildClick={handleClick} />}
            </div>
        </div>
        
        
    )
}

export default Wellcome;