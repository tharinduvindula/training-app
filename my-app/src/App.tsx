import React, { FC, useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Router,
  Route,
  Switch,
  useHistory,
} from "react-router-dom";
import Wellcome from "./wellcome/wellcome";
import Home from "./home/home";
import history from "./utils/history";
import jwt from 'jwt-decode';



const App: FC = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    if (localStorage.getItem('token')) {
      const token:any = localStorage.getItem('token')
      const decodedToken: any = jwt(token.split('JWT')[1]);
      var dateNow = new Date();

      if (decodedToken.exp > dateNow.getTime()/1000) {
        setIsLoggedIn(true);
      } else {
        localStorage.clear()
        setIsLoggedIn(false);
      }
      
    } else {
      setIsLoggedIn(false)
    }
    
  }, [])

  const logOut = (val: boolean) => {
    console.log(val)
    if(val == true){
      console.log("isLoggedIn" + isLoggedIn)
      setIsLoggedIn(false)
    } else {
      console.log("isLoggedIn" + isLoggedIn)
      setIsLoggedIn(true)
    }

  }
  
  if (isLoggedIn) {
    // when load user loged to the system
    history.push('/home')
    return (
      <div>
        <Router history={history}>
          <Switch>
            <Home onChildClick={logOut} />
            <Route path="/home" component={Home} />
          </Switch>
        </Router>
      </div>
    )
    
  } else {
    // when load user not login to the system
    history.push('/')
    return (
      <div>
        <Router history={history}>
          <Switch>
            <Wellcome onChild1Click={logOut} />
            <Route  path="/" component={Wellcome} />
          </Switch>
        </Router>
      </div>
    )
  }
  
  
  
  // return (
  //   <Router>
  //     <Home />
  //     <Switch>
  //       <Route path="/">
  //         <Wellcome />
  //       </Route>
  //       <Route path="/home">
  //         <Home />
  //       </Route>
  //     </Switch>
  //   </Router>
  // );
}


export default App;
