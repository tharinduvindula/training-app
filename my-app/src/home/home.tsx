
import React, { forwardRef, useState, useRef, useImperativeHandle } from 'react';
import './home.css';
import { Modal, Button, Layout, Menu, Form, Input, Select } from 'antd';
import { UploadOutlined, UserOutlined, VideoCameraOutlined, PlusOutlined} from '@ant-design/icons';
import back from '../assets/back.jpg'
import UserTable from './user-table/user-table';
import axios from 'axios';
const { Option } = Select;
const { Header, Content, Footer, Sider } = Layout;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

interface ChildRef { 
    showAlert: any   
}
interface HomeComponentProps {
    onChildClick: (val:boolean) => void
}

const Home: React.FC<HomeComponentProps> = ({ onChildClick}) => {
    const [form] = Form.useForm();
    const childRef = useRef();
    const [visible,setVisible] = useState<boolean>(false)
    const [lastEmail, setLastEmail] = useState<string>('')
    const [confirmLoading, setConfirmLoading] = useState<boolean>(false)
    

    const showModal = () => {
        
        setVisible(true);
    }

    const handleOk = () => {
        setConfirmLoading(true);
        setTimeout(() => {
            setConfirmLoading(false);
            setVisible(false);
        }, 2000);
    }

    const handleCancel = () => {
        setVisible(false);
    }

    const onFinish = (values: any) => {
        setConfirmLoading(true);
        console.log('Success:', values);
        console.log(values);
        setTimeout(() => {
            
        }, 2000);
        axios.post(`http://localhost:3001/api/users/add`, values, {
            headers: {
                'Authorization': `bearer ${localStorage.getItem('token')?.split('JWT')[1]}`
            }})
            .then((res) => {
                console.log(res.data);
                alert("user added");
                setLastEmail(values.email);
                setConfirmLoading(false);
                setVisible(false);
            }).catch(() => {
                alert("user exsit")
                setConfirmLoading(false);
            })
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    function handleChange(value: string) {
        console.log(`selected ${value}`);
        // setValue({ gender : value });
    }

    const logOut = () =>{
        localStorage.clear();
        onChildClick(true);
    }
    

    return (
        // antd fulllayout for home
        <Layout style={{height:"100vh"}}>
             {/* antd slider for home menu */}
            <Sider
                style={{backgroundColor:"#463eff"}}
                breakpoint="lg"
                collapsedWidth="0"
                onBreakpoint={broken => {
                    console.log(broken);
                }}
                onCollapse={(collapsed, type) => {
                    console.log(collapsed, type);
                }}
            >
                <div className="logo" />
                <div style={{paddingTop:"15%",paddingBottom:"15%"}}>
                    <h1 style={{color:"white",textAlign:"center",textTransform: "uppercase",fontSize: "x-large"}} >My System</h1>
                </div>
                <Menu theme="dark" style={{backgroundColor:"#463eff"}} mode="inline" defaultSelectedKeys={['4']}>
                    
                    <Menu.Item key="1" >
                        <h3 style={{ color: "white" }}>Users</h3>
                    </Menu.Item>
                    <Menu.Item key="2" >
                        <h3 style={{ color: "white" }} onClick={() => { {logOut();}}}>Logout</h3>
                    </Menu.Item>
                </Menu>

            </Sider>
            <Layout>
                {/* antd header for home header */}
                <Header className="site-layout-sub-header-background" 
                style={{ padding: 0,borderBlockColor:"black",display:"flex",justifyContent:"flex-end" }} >
                    
                    <div> <img src={back} style={{width:"50px",height:"50px",marginRight:"10px",borderRadius:"50px"}} alt=""/></div>
                    <div style={{ marginRight: "30px"}} ><h2>Tharindu Vinduls</h2></div>
                </Header>
                {/* antd content for home  */}
                <Content style={{ margin: '24px 16px 0' }}>
                    <div className="site-layout-background" style={{ padding: 24, minHeight: "97%" }}>
                        <UserTable lastEmail={lastEmail}/>
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center', display: "flex", justifyContent: "flex-end"}}>
                    <div>
                        <Button style={{ borderRadius: 100 }} type="primary" onClick={showModal}>
                            <PlusOutlined />
                        </Button>
                    </div>

                </Footer>
            </Layout>
            <Modal
                title="Title"
                visible={visible}
                onOk={()=>{form.submit()}}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
            >
                <div style={{ flex: 2, textAlign: "center" }}>
                    <Form
                        {...layout}
                        name="basic"
                        form ={form}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                    >
                        <Form.Item

                            name="name"
                            rules={[{ required: true, message: 'Please input Your name!' }]}
                        >
                            <Input placeholder="Name" />
                        </Form.Item>

                        <Form.Item

                            name="email"
                            rules={[{ type: "email", required: true, message: 'Please input valid Email!' }]}
                        >
                            <Input placeholder="Email" />
                        </Form.Item>

                        <Form.Item

                            name="age"
                            rules={[{ required: true, message: 'Please input valid Age!' }]}
                        >
                            <Input type="number" placeholder="Age" />
                        </Form.Item>

                        <Form.Item

                            name="gender"
                            rules={[{ required: true, message: 'Please input Your gender!' }]}
                        >
                            <Select placeholder="Gender" style={{ width: "100%" }} onChange={handleChange}>
                                <Option value="m">Male</Option>
                                <Option value="f">Female</Option>
                            </Select>

                        </Form.Item>

                        <Form.Item

                            name="address"
                            rules={[{ required: true, message: 'Please input your Address!' }]}
                        >
                            <Input placeholder="Address" />
                        </Form.Item>

                        <Form.Item

                            name="dob"
                            rules={[{ type: "date", required: true, message: 'Please input valid Date!( mm/dd/yyyy)' }]}
                        >
                            <Input placeholder="Date of Birth (mm/dd/yyyy)" />
                        </Form.Item>
                    </Form>
                </div>

            </Modal>
        </Layout>
    )
}


export default Home;



