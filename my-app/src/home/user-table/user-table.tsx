import React, { useEffect, useState } from 'react';
import './user-table.css';
import axios from 'axios';

import { Table, Tag, Space, Input } from 'antd';

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Email',
        key: 'email',
        dataIndex: 'email',
        
    },
    {
        title: 'Gender',
        key: 'gender',
        dataIndex: 'gender',
        
    },
    {
        title: 'Date of Birth',
        key: 'dob',
        dataIndex: 'dob',

    },
];


interface UserTableComponentProps {
    lastEmail:string
}


const UserTable: React.FC<UserTableComponentProps> = ({lastEmail}) => {

    
    const [rusers, setRUser] = useState([])
    const [users, setUser] = useState([])

    useEffect(() => {
        axios.get(`http://localhost:3001/api/users`,{
            headers: {
                'Authorization': `bearer ${localStorage.getItem('token')?.split('JWT')[1]}`
            }
        })
            .then((res: any) => {
                const persons = res.data;
                setUser(persons);
                setRUser(persons)
            })
    }, [lastEmail])

    const search = (value: string) => {
        const  baseData = rusers;
        console.log("PASS", { value });

        const filterTable = baseData.filter((o: { [x: string]: any; }) =>
            Object.keys(o).some(k =>
                String(o[k])
                    .toLowerCase()
                    .includes(value.toLowerCase())
            )
        );

        setUser(filterTable);
    };
    

    return (
        <div style={{maxHeight:"80%"}}>
            <Input.Search
                style={{  margin: "0 0 10px 0" }}
                placeholder="Search by..."
                enterButton
                onSearch={search}
            />
            <Table columns={columns} dataSource={users} />
        </div>
    )
}


export default UserTable;

