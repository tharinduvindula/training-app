import express from "express";
import bodyParser from "body-parser";
const cors = require('cors');
const router = express.Router();
const session = require("express-session");
const passport = require("passport");
const users = require("./routes/users");
const DatabaseService = require("./service/database");
const databaseService = new DatabaseService();
// const pg = require("pg");

// const pgPool = new pg.Pool({
//     user: "postgres",
//     host: "localhost",
//     database: "mydb",
//     password: "1451",
//     port: 5432,
// });

const app = express();
const port = 3001;
databaseService.client.connect()

app.use(session({
    // store: new (require("connect-pg-simple")(session))({
    //     pool : pgPool,                // Connection pool
    //     tableName : "user_sessions",   // Use another table-name than the default "session" one
    // }),
    name: "sid",
    resave:false,
    saveUninitialized: false,
    secret:"secret",
    cookie: {
        maxAge: 1000 * 60 * 60 * 2,
        sameSite: true,
        secure: true,

    }
}))

app.use(cors());

app.use(passport.initialize());
app.use(passport.session());
require('./service/passport')(passport);

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    }),
);
app.use("/api/users", users);

app.get("/", (req, res) => {
    res.send("The sedulous hyena ate the antelope!");
});

app.listen(port, (err: any) => {
    if (err) {
        return console.error(err);
    }
    return console.log(`server is listening on ${port}`);
});
