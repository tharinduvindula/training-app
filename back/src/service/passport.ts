// tslint:disable-next-line: no-var-requires
const JwtStrategy = require("passport-jwt").Strategy;
// tslint:disable-next-line: no-var-requires
const ExtractJwt = require("passport-jwt").ExtractJwt;
import {  QueryResult } from "pg";
const DatabaseService = require('./database');
const databaseService = new DatabaseService();
databaseService.client.connect();


module.exports = (passport) => {
    const opts = {
        jwtFromRequest: null,
        secretOrKey: null,
    };
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken("JWT");
    opts.secretOrKey = "secret";
    passport.use(new JwtStrategy(opts,
        // tslint:disable-next-line: variable-name
        (jwt_payload, done) => {
            const query = "SELECT * FROM users WHERE email=$1";
            databaseService.client.query(query, [jwt_payload.email], (error, user) => {
                if (error) {
                    console.log(error)
                    return done(error, false);
                }

                if (user.rows[0]) {
                    
                    return done(null, user.rows[0]);

                } else {
                    console.log('ff')
                    return done(null, false);
                }
            });
        }));
};
