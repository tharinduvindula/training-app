const express = require("express");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');

const DatabaseService = require("../service/database");
const databaseService = new DatabaseService();
databaseService.client.connect();
// tslint:disable-next-line: no-var-requires
const JwtStrategy = require("passport-jwt").Strategy;
// tslint:disable-next-line: no-var-requires
const ExtractJwt = require("passport-jwt").ExtractJwt;
import { QueryResult } from "pg";

// get all users
router.get("/", passport.authenticate('jwt', { session: false }) , (req, res) => {
    
    databaseService.client.query("SELECT * FROM users ORDER BY id ASC", (error, results) => {
        if (error) {
            
            return res.status(400).json({ error: "error :- " + error });
        }
        let sResult = []
        results.rows.forEach(e=> {
            if (e.dob !== null) {
                
                e.dob = new Date(e.dob).getFullYear() + '-' + new Date(e.dob).getMonth() + '-' + new Date(e.dob).getDate();
            }
            sResult.push(e)
        })
        return res.status(200).json(sResult);
    });
});


router.post("/", (req, res) => {

    // for register
    if (req.body.password !== undefined && req.body.name !== undefined && req.body.email !== undefined) {
        const query = `CALL user_register($1, $2,$3)`;
        bcrypt.hash(req.body.password, 0, (err, hash) =>{

           
            databaseService.client.query(query, [req.body.email, req.body.name, hash], (error, results) => {
                if (error) {
                    console.log(query);
                    console.log(error);
                    
                    return res.status(400).json({ error: "error :- " + error });
                }
                
                return res.status(200).json(results);
            });
            console.log(err)

        });
        

    } else if (req.body.password !== undefined && req.body.email !== undefined) { // for login
        const query = "SELECT * FROM users WHERE email=$1";
        const query1 = "SELECT * FROM password WHERE email=$1";
        databaseService.client.query(query, [req.body.email], (error, results) => {
            if (error) {
                console.log(error)
                return res.status(400).json({ error: "error :- " + error });
            } else {
                databaseService.client.query(query1, [req.body.email], (error1, results1) => {
                    if (error1) {
                        return res.status(400).json({ error: "error :- " + error });
                    } else {
                        bcrypt.compare(req.body.password, results1.rows[0].password,  (err, result) => {
 
                            if(result == true){
                                const userToken = jwt.sign(({
                                    "id": results.rows[0].id,
                                    "name": results.rows[0].name,
                                    "email": results.rows[0].email,
                                }), "secret", {
                                    expiresIn: 604500
                                });

                                return res.json({
                                    userToken: "JWT" + userToken,
                                    success: true,
                                    msg: "sign in",

                                });

                            } else {
                    
                                return res.status(400).json({
                                    success: false,
                                    msg: "possword dosent match",
                                });
                            }
                        });
                        
                    }
                });
            }
        });
    }
});


router.post("/add", passport.authenticate('jwt', {session: false}) ,(req, res) => {
    
    
    // const age = req.body.age === undefined ? null : req.body.age;
    const age = req.body.age?? null;
    const address = req.body.address === undefined ? null : req.body.address;
    const gender = req.body.gender === undefined ? null : req.body.gender;
    const dob = req.body.dob === undefined ? null : req.body.dob.split('/')[2] + '-' + req.body.dob.split('/')[0] + '-' + req.body.dob.split('/')[1] +'T5:30:00.000Z';

    const query = `INSERT INTO public.users(id, email, name,age,address,gender,dob) 
                VALUES (nextval('user_sequence'),$1,$2,$3,$4,$5,$6)`;
    databaseService.client.query(query,
        [req.body.email, req.body.name, age, address, gender, dob],
        (error, results) => {
            if (error) {
                return res.status(400).json({ error: "error :- " + error });
            }
            return res.status(200).json(results);
        });

});

module.exports = router;

// CREATE OR REPLACE PROCEDURE  user_register(
// 	character varying,
// 	character varying,
// 	character varying
// )
// LANGUAGE 'plpgsql'
// AS $$
// DECLARE
// 	user_id INT;
// BEGIN

//   	INSERT INTO public.users(id, email, name)
//   	VALUES(nextval('user_sequence'), $1, $2)
//    	RETURNING id INTO user_id;

//   	INSERT INTO public.password(id, email, password, user_id)
//   	VALUES(nextval('password_sequence'),  $1, $3, user_id);

// 	COMMIT;

// -- EXCEPTION 
// -- 	WHEN OTHERS THEN
// -- 	ROLLBACK;

// END;
// $$;

// CALL user_register('cc', 'kk', 'kkk');
